/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/isohash',
  version: '2.0.1',
  description: 'a hash package that works cross environment'
}
