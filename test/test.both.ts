import { expect, tap } from '@pushrocks/tapbundle';
import * as isohash from '../ts/index.js';

tap.test('first test', async () => {
  const compareHash = 'f94b840e404ef088fd378ad3811e36357ab6ee39020bfbe48c8bf11b65d100dc';
  const originString = 'eruzw873zrhchkjhdfuezrw';
  const hashResult = await isohash.sha256FromString(originString);
  expect(hashResult).toEqual(compareHash);
  console.log(hashResult);
});

tap.start();
